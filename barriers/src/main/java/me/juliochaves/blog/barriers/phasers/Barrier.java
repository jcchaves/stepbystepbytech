package me.juliochaves.blog.barriers.phasers;

import java.util.function.Consumer;

public interface Barrier {

    void enter();

    void leave();

    void await();

    void onComplete(Consumer<String> onCompleteCallback);

}

package me.juliochaves.blog.barriers.phasers;

import java.util.concurrent.Phaser;
import java.util.function.Consumer;

public class ForkJoinBarrier implements Barrier {

    private Phaser phaser;
    private Consumer<String> onCompleteCallback;

    public ForkJoinBarrier() {
        this.phaser = new Phaser(1){
            @Override
            protected boolean onAdvance(int phase, int registeredParties) {
                doOnComplete("Barrier: All tasks in barrier have finished");
                return super.onAdvance(phase, registeredParties);
            }
        };
    }

    @Override
    public void enter() {
        if(!this.phaser.isTerminated()) {
            this.phaser.register();
        } else {
            throw new IllegalStateException("This fork barrier has been already used, no new parties can join");
        }
    }

    @Override
    public void leave() {
        if(!this.phaser.isTerminated()) {
            this.phaser.arriveAndDeregister();
        } else {
            throw new IllegalStateException("This fork barrier has been already used, no parties can leave now");
        }
    }

    @Override
    public void await() {
        if(!this.phaser.isTerminated()) {
            this.phaser.arriveAndAwaitAdvance();
        } else {
            throw new IllegalStateException("This fork barrier has been already used, cannot wait on it anymore");
        }
    }

    @Override
    public void onComplete(Consumer<String> onCompleteCallback) {
        this.onCompleteCallback = onCompleteCallback;
    }

    private void doOnComplete(String result) {
        this.onCompleteCallback.accept(result);
    }
}

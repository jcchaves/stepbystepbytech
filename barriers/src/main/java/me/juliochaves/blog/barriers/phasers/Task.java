package me.juliochaves.blog.barriers.phasers;

import io.vavr.control.Try;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Task implements Runnable {

    private long id;
    private long processingTimeMillis;
    private Barrier barrier;
    private Consumer<String> onCompleteCallback;

    public Task(long id, long processingTimeMillis, Barrier barrier) {
        this.id = id;
        this.processingTimeMillis = processingTimeMillis;
        this.barrier = barrier;
        this.barrier.enter();
    }

    @Override
    public void run() {
        Try.run(() -> TimeUnit.MILLISECONDS.sleep(this.processingTimeMillis))
                .onFailure(t -> {
                    throw new RuntimeException(t);
                });
        doOnComplete("Task [" + this.id + "] finished");
        this.barrier.leave();
    }

    public void onComplete(Consumer<String> onCompleteCallback) {
        this.onCompleteCallback = onCompleteCallback;
    }

    private void doOnComplete(String result) {
        this.onCompleteCallback.accept(result);
    }
}

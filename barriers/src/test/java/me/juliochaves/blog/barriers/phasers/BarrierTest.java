package me.juliochaves.blog.barriers.phasers;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class BarrierTest {

    @Test
    public void dependantTaskCanWaitForDependenciesTasksToCompleteUsingBarrier() {
        //Arrange
        final List<String> results = Collections.synchronizedList(new ArrayList<>());

        Consumer<String> tasksResultsAggregator = taskResult -> {
            results.add(taskResult);
        };

        ForkJoinBarrier forkBarrier = new ForkJoinBarrier();
        forkBarrier.onComplete(tasksResultsAggregator);

        Task task1 = buildTask(1, 1000, forkBarrier, tasksResultsAggregator);
        Task task2 = buildTask(2, 1000, forkBarrier, tasksResultsAggregator);
        Task task3 = buildTask(3, 1000, forkBarrier, tasksResultsAggregator);
        Task task4 = buildTask(4, 1000, forkBarrier, tasksResultsAggregator);
        Task task5 = buildTask(5, 1000, forkBarrier, tasksResultsAggregator);

        ExecutorService threadPoolExecutorService = Executors.newFixedThreadPool(5);

        //Act

        threadPoolExecutorService.execute(task1);
        threadPoolExecutorService.execute(task2);
        threadPoolExecutorService.execute(task3);
        threadPoolExecutorService.execute(task4);
        threadPoolExecutorService.execute(task5);

        forkBarrier.await();

        //Assert
        assertThat(results.subList(0, 5), everyItem(startsWith("Task")));
        assertThat(results.get(5), equalTo("Barrier: All tasks in barrier have finished"));

    }

    private Task buildTask(int id, long processingTimeMillis, Barrier barrier, Consumer<String> onCompleteCallback) {
        Task task = new Task(id, processingTimeMillis, barrier);
        task.onComplete(onCompleteCallback);
        return task;
    }
}

package me.juliochaves.blog.nio.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.CharsetUtil;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class ClientApp {

    public static void main(String[] args) {

        SslContext sslContext = null;
        Path dummyJksPath = Paths.get("nio-netty/src/main/resources/dummy_client.jks");

        System.out.println(dummyJksPath.toAbsolutePath());

        try (FileInputStream fis = new FileInputStream(dummyJksPath.toFile())) {

            KeyStore dummyKeystore = KeyStore.getInstance("JKS");
            char[] dummyKSPwd = "clientks123456".toCharArray();
            dummyKeystore.load(fis, dummyKSPwd);
            TrustManagerFactory trstmf = TrustManagerFactory.getInstance("SunX509");
            trstmf.init(dummyKeystore);

            sslContext = SslContextBuilder.forClient().trustManager(trstmf).build();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        EventLoopGroup group = null;
        try {
            group = new NioEventLoopGroup();
            Bootstrap bootstrap = new Bootstrap();
            bootstrap
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ClientChannelInitializer(sslContext))
                    .remoteAddress("localhost", 8888);
            ChannelFuture channelFuture = null;

            channelFuture = bootstrap.connect().sync();
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                group.shutdownGracefully().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private static final class ClientChannelInitializer extends ChannelInitializer<NioSocketChannel> {

        private SslContext sslContext;

        public ClientChannelInitializer(SslContext sslContext) {
            this.sslContext = sslContext;
        }

        @Override
        protected void initChannel(NioSocketChannel ch) throws Exception {
            SSLEngine engine = this.sslContext.newEngine(ch.alloc());
            engine.setEnabledCipherSuites(new String[]{"TLS_RSA_WITH_AES_256_CBC_SHA256","TLS_RSA_WITH_AES_256_CBC_SHA"});
            ch.pipeline()
                    .addFirst("ssl", new SslHandler(engine))
                    .addLast(new EchoClientHandler());
        }
    }

    private static final class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

        @Override
        public void channelActive(ChannelHandlerContext ctx) throws Exception {
            ctx.writeAndFlush(Unpooled.copiedBuffer("Hello!", CharsetUtil.UTF_8));
        }

        @Override
        protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
            ByteBuf in = (ByteBuf) msg;
            System.out.println("Client received: " + in.toString(CharsetUtil.UTF_8));
            ChannelFuture chf = ctx.channel().close().addListener(f -> {
                if (f.isSuccess()) {
                    System.out.println("Closing client channel");
                } else {
                    System.out.println("Could not close client channel");
                }
            });
        }

        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {

        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            cause.printStackTrace();
        }
    }

}

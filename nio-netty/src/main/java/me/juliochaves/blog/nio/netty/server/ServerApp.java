package me.juliochaves.blog.nio.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.Future;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLEngine;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class ServerApp {

    public static void main(String[] args) {
        SslContext sslContext = null;
        Path dummyJksPath = Paths.get("nio-netty/src/main/resources/dummy_server.jks");

        System.out.println(dummyJksPath.toAbsolutePath());

        try (FileInputStream fis = new FileInputStream(dummyJksPath.toFile())){

            KeyStore dummyKeystore = KeyStore.getInstance("JKS");
            char[] dummyKSPwd = "serverks123456".toCharArray();
            dummyKeystore.load(fis, dummyKSPwd);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(dummyKeystore, dummyKSPwd);

            sslContext = SslContextBuilder.forServer(kmf).build();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }

        EventLoopGroup group = new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap
                .group(group)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ServerChannelInitializer(sslContext));
        ChannelFuture channelFuture = bootstrap.bind(8888);
        channelFuture.addListener(chf -> {
            if (chf.isSuccess()) {
                System.out.println("Server has bound correctly");
            } else {
                System.err.println("Error while trying to bind");
            }
        });
        try {
            channelFuture.sync();
        } catch (InterruptedException e) {
            System.err.println("Error while waiting for server bind to finish");
            e.printStackTrace();
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                Future<?> future = group.shutdownGracefully();
                future.syncUninterruptibly();
                System.out.println("Event group shutdown successfully");
            }
        });
    }

    private static final class ServerChannelInitializer extends ChannelInitializer<NioSocketChannel> {

        private SslContext sslContext;

        public ServerChannelInitializer(SslContext sslContext) {
            this.sslContext = sslContext;
        }

        @Override
        protected void initChannel(NioSocketChannel ch) throws Exception {
            SSLEngine engine = this.sslContext.newEngine(ch.alloc());
            engine.setEnabledCipherSuites(new String[]{"TLS_RSA_WITH_AES_256_CBC_SHA256","TLS_RSA_WITH_AES_256_CBC_SHA"});
            ch.pipeline()
                    .addFirst("ssl", new SslHandler(engine))
                    .addLast(new EchoServerHandler());
        }
    }

    private static final class EchoServerHandler extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            ByteBuf in = (ByteBuf) msg;
            System.out.println("Server received: " + in.toString(CharsetUtil.UTF_8));
            ctx.write(in);
        }

        @Override
        public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
            super.userEventTriggered(ctx, evt);
        }

        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER);
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            cause.printStackTrace();
        }
    }
}

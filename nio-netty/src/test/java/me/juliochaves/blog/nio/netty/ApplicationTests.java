package me.juliochaves.blog.nio.netty;

import static org.junit.jupiter.api.Assertions.*;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;

public class ApplicationTests {

    @Test
    public void canModifyByteBufContent() {
        ProtobufEncoder encoder = new ProtobufEncoder();

        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf sliced = buf.slice(0, 14);
        System.out.println(sliced.toString(utf8));
        buf.setByte(0, (byte) 'J');
        assertTrue(buf.getByte(0) == sliced.getByte(0));
    }

    @Test
    public void canCopyByteBufContent() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf copy = buf.copy(0, 14);
        System.out.println(copy.toString(utf8));
        buf.setByte(0, (byte) 'J');
        assertTrue(buf.getByte(0) != copy.getByte(0));
    }
}

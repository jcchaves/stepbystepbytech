#!/bin/sh
MAXRAM=$(expr `cat /sys/fs/cgroup/memory/memory.limit_in_bytes` / 1024 / 1024) && \
MAXRAM=$(($MAXRAM))m && \
echo "MaxRam: $MAXRAM" && \
exec java -XX:+PrintFlagsFinal -cp app:app/lib/* me.juliochaves.blog.spring.web.echoservice.Application "$@"
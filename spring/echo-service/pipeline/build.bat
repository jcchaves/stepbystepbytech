@echo off
call setenv.bat
SET PIPELINE_DIR=%cd%
cd ..
SET WORK_DIR=%cd%
echo "================================="
echo "Building artifact"
start /wait /b cmd /c mvn clean package
echo "================================="
echo "Decompressing JAR file"
cd target
mkdir dependency
copy *.jar dependency
cd dependency
start /wait /b cmd /c jar -xf *.jar
echo "================================="
echo "Building docker image"
cd %WORK_DIR%
start /wait /b cmd /c docker build --rm -t %DOCKER_REPOSITORY%/%DOCKER_IMAGE_NAME%:%DOCKER_IMAGE_TAG% .
echo "================================="
echo "Pushing image to docker repository"
start /wait /b cmd /c docker push %DOCKER_REPOSITORY%/%DOCKER_IMAGE_NAME%:%DOCKER_IMAGE_TAG%
echo "================================="
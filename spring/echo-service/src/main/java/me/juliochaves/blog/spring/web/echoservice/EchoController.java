package me.juliochaves.blog.spring.web.echoservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/")
public class EchoController {

    @RequestMapping(path = {"", "/"})
    public EchoRequest echo(HttpServletRequest req) {
        return buildEchoRequest(req);
    }

    private EchoRequest buildEchoRequest(HttpServletRequest req) {

        Map<String, Object> headers = new HashMap<>();
        Enumeration<String> headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headers.put(headerName, req.getHeader(headerName));
        }

        Map<String, List<String>> parameters = new HashMap<>();
        Map<String, String[]> parameterMap = req.getParameterMap();
        for (Map.Entry<String, String[]> parameter : parameterMap.entrySet()) {
            parameters.put(parameter.getKey(), Arrays.asList(parameter.getValue()));
        }

        return EchoRequest.builder()
                .headers(headers)
                .parameters(parameters)
                .build();
    }
}

package me.juliochaves.blog.spring.web.echoservice;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@Builder
@Getter
public class EchoRequest {

    private Map<String, Object> headers;
    private Map<String, List<String>> parameters;

}

package me.juliochaves.blog.data.streaming.storm;

import org.apache.storm.topology.ConfigurableTopology;
import org.apache.storm.topology.TopologyBuilder;

public class Application extends ConfigurableTopology {

    public static void main(String[] args) {
        ConfigurableTopology.start(new Application(), args);
    }

    @Override
    protected int run(String[] args) throws Exception {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("words", new WordSpout(), 1);
        builder.setBolt("uppercase", new UpperCaseBolt(), 1)
                .shuffleGrouping("words");
        builder.setBolt("logger", new LoggerBolt(), 1)
                .shuffleGrouping("uppercase");

        conf.setDebug(true);

        String topologyName = "word-uppercase";

        conf.setNumWorkers(1);

        if (args != null && args.length > 0) {
            topologyName = args[0];
        }
        return submit(topologyName, conf, builder);
    }
}
